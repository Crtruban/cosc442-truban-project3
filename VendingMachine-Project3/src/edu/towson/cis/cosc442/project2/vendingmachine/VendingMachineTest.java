package edu.towson.cis.cosc442.project2.vendingmachine;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;



import static org.junit.jupiter.api.Assertions.assertThrows;
public class VendingMachineTest {

	VendingMachine vend1, vend2;
	VendingMachineItem vItem1, vItem2, vItem3, vItem4;
	@Before
	public void setUp() throws Exception 
	{
	vend1 = new VendingMachine();
	vend2 = new VendingMachine();
	vItem1 = new VendingMachineItem("Hershey's Bar", 1.50);
	vItem2 = new VendingMachineItem("Twix", 2.00);
	vItem3 = new VendingMachineItem("Starburst", 1.00);
	vItem4 = new VendingMachineItem("Skittles", 1.75);
	}
	/**
	 * Run the int getSlot() method test.
	 * 
	 * @throws Exception
	 */
	
	@Test
	public void testGetSlot_1()
	throws Exception{
		VendingMachine fixture = new VendingMachine();
		assertEquals(fixture.getSlotIndex("B"),1);
		assertEquals(fixture.getSlotIndex("C"),2);
		assertEquals(fixture.getSlotIndex("D"),3);
		
	}
	/**
	 * Testing the addItem() method from {@link VendingMachine} class.
	 */


	@Test
	public void testAddItem() 
	{
		vend1.addItem(vItem1, "A");
		assertEquals(vItem1, vend1.getItem("A"));
		vend2.addItem(vItem3, "C");
		assertEquals(vItem3, vend2.getItem("C"));
		

	}
	
	/**
	 * Testing for exceptions in addItem() method from {@link VendingMachine} class 
	 */
	@Test(expected = VendingMachineException.class)
	public void testExceptionAddItem()
	{
		vend1.addItem(vItem1, "A");
		vend1.addItem(vItem2, "A");
		vend1.addItem(vItem3,"A");
		vend2.addItem(vItem1, "A");
		vend2.addItem(vItem2, "B");
		vend2.addItem(vItem3,"B");
	}
	@Test(expected = VendingMachineException.class)
	public void testExceptionGetSlotIndex()
	{
		vend1.getSlotIndex("G");
	}
	/**
	 * Testing the removeItem() method from {@link VendingMachine} class.
	 */
	@Test
	public void testRemoveItem() {
		vend1.addItem(vItem1, "B");
		vend1.addItem(vItem2, "A");
		assertEquals(null, vend1.removeItem("B"));
		assertEquals(null, vend1.removeItem("A"));
	}
	
	/**
	 * Testing exceptions for removeItem() method from {@link VendingMachine} class.
	 */
	@Test(expected = VendingMachineException.class)
	public void testExceptionRemoveItem()
	{
		vend1.addItem(vItem1, "A");
		vend1.removeItem("A");
		vend1.removeItem("A");
	}
	/**
	 * Testing the getBalance() method from {@link VendingMachine} class.
	 *Initial Balance for vending machine is 0
	 */
	@Test
	public void testGetBalance() {
		assertEquals(0,vend1.getBalance(), 0.0001);
		assertEquals(0,vend2.getBalance(), 0.0001);
	}
	/**
	 * Testing the InsertMoney() method from {@link VendingMachine} class.
	 */
	@Test
	public void testInsertMoney() {
		vend1.insertMoney(30);
		assertEquals(30, vend1.getBalance(), 0.0001);
		vend2.insertMoney(10);
		assertEquals(10, vend2.getBalance(), 0.0001);
		
	}
	/**
	 * Testing exceptions in the InsertMoney() method from {@link VendingMachine} class.
	 */
	@Test(expected = VendingMachineException.class)
	public void testExceptionInsertMoney()
	{
		vend1.insertMoney(-20);
		vend1.insertMoney(0);
		vend2.insertMoney(-50);
		vend2.insertMoney(0);
	}

	/**
	 * Testing the MakePurchase() method from {@link VendingMachine} class.
	 */
	@Test
	public void testMakePurchase() {
		vend1.addItem(vItem1, "A");
		vend1.insertMoney(2.0);
		assertEquals(true,vend1.makePurchase("A"));
		vend2.addItem(vItem3,"C");
		vend2.insertMoney(3);
		assertEquals(true,vend2.makePurchase("C"));
	}
	/**
	 * Testing the ReturnChange() method from {@link VendingMachine} class.
	 */
	@Test
	public void testReturnChange() 
	{
		vend1.addItem(vItem1, "A");
		vend1.insertMoney(2.0);
		vend1.makePurchase("A");
		assertEquals(.5,vend1.returnChange(),0.001);
	}
	@After
	public void tearDown() throws Exception 
	{
		vend1 = null;
		vend2 = null;
	}
}
